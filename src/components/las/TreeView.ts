class NavigationTreeView extends HTMLElement {
    static template = `
<ul>
  <li>Anträge
      <ul>
        <li id="id0815">0815</li>
        <li id="id4711">4711</li>
      </ul>
  </li>
  <li>Verträge</li>
  <li>Schäden</li>
</ul>
`;

    node: any;
    nodeId2ViewIds: any;
    viewId2View: any;

    createdCallback() {
        // XXX constructor is never called
        this.init();
        this.render();
        this.addHandlers();
    }

    private render() {
        this.createShadowRoot().innerHTML = NavigationTreeView.template;
    };

    private init() {
        this.node = {};
        this.nodeId2ViewIds = {};
        this.viewId2View = {};
    };

    private addHandlers() {
        const that = this;
        const item: HTMLElement = this.shadowRoot.querySelector("#id0815");
        item.addEventListener('click', (e: Event) => {
            const target: HTMLElement = <HTMLElement>e.target;
            that.displayInTreeNode('id0815', 'view-one');
        });
        const item2: HTMLElement = this.shadowRoot.querySelector("#id4711");
        item2.addEventListener('click', (e: Event) => {
            const target: HTMLElement = <HTMLElement>e.target;
            that.displayInTreeNode('id4711', 'view-three');
        });
    }

    private displayInTreeNode(nodeId: string, viewId: string) {
        if (this.nodeId2ViewIds[nodeId]) {
            const viewIds = this.nodeId2ViewIds[nodeId];
            this.displayTopView(viewIds);
        } else {
            const view = document.createElement(viewId);
            this.viewId2View[viewId] = view;
            this.nodeId2ViewIds[nodeId] = [viewId];
            this.signalViewChange(view);
        }
    }

    private signalViewChange(view) {
        const event: any = new Event('view-changed');
        event.view = view;
        this.dispatchEvent(event);
    };

    closeView({
        viewId,
        nodeId
        }) {
        if (this.nodeId2ViewIds[nodeId]) {
            const viewIds = this.nodeId2ViewIds[nodeId];
            const idx = viewIds.indexOf(viewId);
            if (idx !== -1) {
                viewIds.splice(idx, 1);
                delete this.viewId2View[viewId];
                if (viewIds.length > 0) {
                    this.displayTopView(viewIds);
                } else {
                    // TODO: how to reset main view
                }
            }
        }
    }

    private displayTopView(viewIds) {
        const topViewId = viewIds[viewIds.length - 1];
        const topView = this.viewId2View[topViewId];
        this.signalViewChange(topView);
    }

    showView({
        viewId,
        nodeId
        }) {
        if (this.nodeId2ViewIds[nodeId]) {
            const viewIds = this.nodeId2ViewIds[nodeId];
            viewIds.push(viewId);
        } else {
            this.nodeId2ViewIds[nodeId] = [viewId];
        }
        const view = document.createElement(viewId);
        this.viewId2View[viewId] = view;
        this.signalViewChange(view);
    }
}
document.registerElement('navigation-tree', NavigationTreeView);