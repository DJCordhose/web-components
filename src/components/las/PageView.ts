class PageView extends HTMLElement {

    static template = `
<style>
#tree {
  float: left;
  border: 1px solid green;
}

#main {
  border: 1px solid blue;
}

</style>
<div id="tree">
  <navigation-tree></navigation-tree>
</div>
<div id="main">
  <main-view></main-view>
</div>
`;

    tree: NavigationTreeView;
    main: MainView;

    createdCallback() {
        this.createShadowRoot().innerHTML = PageView.template;
        this.addHandler();
    }

    addHandler() {
        const that = this;
        this.tree = this.shadowRoot.querySelector('navigation-tree');
        this.main = this.shadowRoot.querySelector('main-view');
        this.tree.addEventListener('view-changed', (e: any) => {
            that.main.displayView(e.view);
        });
        this.main.addEventListener('open', (e: any) => {
            const {viewId, nodeId} = e;
            this.tree.showView({
                viewId,
                nodeId
            });
        });
        this.main.addEventListener('close', (e: any) => {
            const {viewId, nodeId} = e;
            this.tree.closeView({
                viewId,
                nodeId
            });
        });
    }

}
document.registerElement('page-view', PageView);