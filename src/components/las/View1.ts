class View1 extends HTMLElement {
    static template = `
    <div>
      <h1>View1</h1>
      <input type="text">
      <button id="btn">Öffnen</button>
    </div>
    `;

    createdCallback() {
        this.createShadowRoot().innerHTML = View1.template;
        this.addHandler();
    }

    addHandler() {
        const btn: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn');
        btn.addEventListener('click', (e: Event) => {
            const event: any = new Event("open");
            event.viewId = 'view-two';
            event.nodeId = 'id0815';
            this.dispatchEvent(event);
        });

    }
}
document.registerElement('view-one', View1);