/// <reference path="./ControllerView.ts" />

import toggleEdit from '../actions-creators/toggle-edit.ts'
import EditModeStore from '../stores/EditModeStore.ts'

class MainView extends HTMLElement implements ControllerView {
    createdCallback() {
        let template: any = document.querySelector('#main-view-template');
        let clone = document.importNode(template.content, true);
        this.createShadowRoot().appendChild(clone);
        let knopf: any = this.shadowRoot.querySelector('lvm-knopf');
        knopf.addEventListener('toggled', (e: Event) => {
            toggleEdit();
        });
        this.updateChildState();
        EditModeStore.register(this);
    }

    stateChanged(store) {
        this.updateChildState();
    }

    updateChildState() {
        const editMode = EditModeStore.editMode;
        const inputElements = this.shadowRoot.querySelectorAll('lvm-input');
        for (let inputElement of inputElements) {
            inputElement.setAttribute('mode', editMode);
        }
    }

}
document.registerElement('main-view', MainView);