/// <reference path="Action.ts" />

// for test
//import Dispatcher = require('../Dispatcher');
import Dispatcher from '../Dispatcher.ts'
import ToggleEditAction from './ToggleEditAction.ts'

export default function toggleEdit() {
    const action = new ToggleEditAction();
    Dispatcher.dispatch(action);
}
