import expect from 'expect';

import toggleEdit from '../src/actions-creators/toggle-edit'
import EditModeStore from '../src/stores/EditModeStore'


describe('EditMode', () => {
    it('toggle action creator toggles state', () => {
        expect(EditModeStore.editMode).toBe('edit');
        toggleEdit();
        expect(EditModeStore.editMode).toBe('view');
    });
});
