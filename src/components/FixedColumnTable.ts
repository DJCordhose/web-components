const template = `
<style>
.fixed-column {
    display: inline-block;
    overflow-y: scroll;
    /*border: 1px solid blue;*/
    height: 50px;
}

::content table {
    vertical-align: top;
    width: 50px;
    overflow-x: scroll;
    overflow-y: scroll;
    display: inline-block;
    height: 50px;
}

::content table tr th {
    white-space: nowrap;
}
</style>
<table class="fixed-column">
    <thead>
    <tr id="fixed-head">
    </tr>
    </thead>
    <tbody id="fixed-body">
    </tbody>
</table>
<content></content>
`;

class FixedColumnTable extends HTMLElement {
    createdCallback() {
        this.createShadowRoot().innerHTML = template;
        this.init();
    }

    private init() {
        const children = this.getChildren();
        const innerTable = this.getChildByType(children, HTMLTableElement);
        if (innerTable === null) {
            throw new Error('fixed-column-table needs table as child');
        }
        const firstContent = innerTable.querySelectorAll('tr *:first-child');
        const fixedHead = this.shadowRoot.getElementById('fixed-head');
        fixedHead.appendChild(firstContent[0]);
        const fixedBody = this.shadowRoot.getElementById('fixed-body');
        for (let i=1; i < firstContent.length; i++) {
            const tr = document.createElement('tr');
            tr.appendChild(firstContent[i]);
            fixedBody.appendChild(tr);
        }
        this.sync();
    }

    private getChildByType(children, Type) {
        let innerTable = null;
        for (const child of children) {
            if (child instanceof Type) {
                innerTable = child;
            }
        }
        return innerTable;
    };

    private getChildren() {
        const content = this.shadowRoot.querySelector('content');
        const children = content.getDistributedNodes();
        return children;
    };

    private sync() {
        const fixedTable = this.shadowRoot.querySelector(".fixed-column");
        const children = this.getChildren();
        const mainTable = this.getChildByType(children, HTMLTableElement);
        fixedTable.addEventListener('scroll', (e) => {
            const scrollPosition = fixedTable.scrollTop;
            mainTable.scrollTop = scrollPosition;
        });
        mainTable.addEventListener('scroll', (e) => {
            const scrollPosition = mainTable.scrollTop;
            fixedTable.scrollTop = scrollPosition;
        });

    }
}
document.registerElement('fixed-column-table', FixedColumnTable);
