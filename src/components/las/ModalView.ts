class ModalView extends HTMLElement {
    static template = `
    <style>
        div {
            background: rgba(255, 255, 255, 0.5);
            z-index: 1;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <div>
      <h1>ModalView</h1>
      <button id="btn">Close</button>
        <button id="btn2">Show Modal</button>
    </div>
    `;

    isModal: boolean;

    createdCallback() {
        this.isModal = true;
        this.createShadowRoot().innerHTML = ModalView.template;
        this.addHandler();
    }

    addHandler() {
        const that = this;
        const btn: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn');
        btn.addEventListener('click', (e: Event) => {
            that.remove();
            //const event: any = new Event("close");
            //event.viewId = 'modal-view';
            //event.nodeId = 'id4711';
            //this.dispatchEvent(event);
        });
        const btn2: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn2');
        btn2.addEventListener('click', (e: Event) => {
            const element = document.createElement('modal-view');
            //const myIndex = that.style.zIndex;
            //element.style.zIndex = myIndex + 1;
            let myIndex = parseInt(that.style.zIndex);
            if (isNaN(myIndex)) {
                myIndex = 0;
            }
            element.style.zIndex = String(myIndex + 1);
            document.body.appendChild(element);
            //that.appendChild(element);
        });

    }

}
document.registerElement('modal-view', ModalView);