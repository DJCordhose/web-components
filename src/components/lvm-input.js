var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var view = 'view';
var edit = 'edit';
var LvmInput = (function (_super) {
    __extends(LvmInput, _super);
    function LvmInput() {
        _super.apply(this, arguments);
    }
    LvmInput.prototype.createdCallback = function () {
        console.log('createdCallback');
        this.init();
        this.update();
    };
    LvmInput.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
        console.log(name, oldValue, newValue);
        this.update();
    };
    LvmInput.prototype.init = function () {
        this.inputTemplate = document.querySelector('#input-template-input');
        this.labelTemplate = document.querySelector('#input-template-lbl');
        this.inputClone = document.importNode(this.inputTemplate.content, true);
        this.labelClone = document.importNode(this.labelTemplate.content, true);
        this.createShadowRoot();
        this.shadowRoot.appendChild(this.labelClone);
        this.label = this.shadowRoot.getElementById('lbl');
        this.shadowRoot.appendChild(this.inputClone);
        this.inputEl = this.shadowRoot.getElementById("in");
    };
    LvmInput.prototype.update = function () {
        this.mode = this.getAttribute('mode');
        this.text = this.getAttribute('text');
        if (this.mode === view) {
            this.label.style.display = 'inline';
            this.inputEl.style.display = 'none';
        }
        else {
            this.label.style.display = 'none';
            this.inputEl.style.display = 'inline';
        }
        if (this.inputEl) {
            this.inputEl.value = this.text;
        }
        if (this.label) {
            this.label.innerHTML = this.text;
        }
    };
    return LvmInput;
})(HTMLElement);
document.registerElement('lvm-input', LvmInput);
//# sourceMappingURL=lvm-input.js.map