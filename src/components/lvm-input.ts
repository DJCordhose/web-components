var view = 'view';
var edit = 'edit';

interface Document {
    registerElement: any;
}

interface HTMLElement {
    createShadowRoot: any;
    shadowRoot: any;
}

class LvmInput extends HTMLElement {
    private mode: string;
    private text: string;
    private inputTemplate: any;
    private labelTemplate: any;
    private inputClone: any;
    private labelClone: any;
    private label: any;
    private inputEl: any;

    // life cycle method, called when an instance of the component is created
    createdCallback() {
        console.log('createdCallback');
        this.init();
        this.update();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log(name, oldValue, newValue);
        this.update();
    }

    init() {
        this.inputTemplate = <HTMLInputElement>document.querySelector('#input-template-input');
        this.labelTemplate = document.querySelector('#input-template-lbl');
        this.inputClone = document.importNode(this.inputTemplate.content, true);
        this.labelClone = document.importNode(this.labelTemplate.content, true);
        this.createShadowRoot();
        this.shadowRoot.appendChild(this.labelClone);
        this.label = this.shadowRoot.getElementById('lbl');
        this.shadowRoot.appendChild(this.inputClone);
        this.inputEl = this.shadowRoot.getElementById("in");
    }

    update() {
        this.mode = this.getAttribute('mode');
        this.text = this.getAttribute('text');
        if (this.mode === view) {
            this.label.style.display = 'inline';
            this.inputEl.style.display = 'none';
        } else {
            this.label.style.display = 'none';
            this.inputEl.style.display = 'inline';
        }
        if (this.inputEl) {
            this.inputEl.value = this.text;
        }
        if (this.label) {
            this.label.innerHTML = this.text;
        }
    }


}
document.registerElement('lvm-input', LvmInput);
