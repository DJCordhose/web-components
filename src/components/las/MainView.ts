class MainView extends HTMLElement {

    static template = `
<div></div>
`;

    private content: HTMLElement ;

    createdCallback() {
        this.createShadowRoot().innerHTML = MainView.template;
        this.content = this.shadowRoot.querySelector('div');
    }

    displayView(view: any) {
        if (!view.isModal) {
            this.resetViews();
        }
        this.content.appendChild(view);
    }

    private resetViews() {
        while (this.content.firstChild) {
            this.content.removeChild(this.content.firstChild);
        }
    }
}
document.registerElement('main-view', MainView);