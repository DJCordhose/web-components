class View2 extends HTMLElement {
    static template = `
    <div>
      <h1>View2</h1>
      <button id="btn">Close</button>
    </div>
    `;

    createdCallback() {
        this.createShadowRoot().innerHTML = View2.template;
        this.addHandler();
    }

    addHandler() {
        const btn: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn');
        btn.addEventListener('click', (e: Event) => {
            const event: any = new Event("close");
            event.viewId = 'view-two';
            event.nodeId = 'id0815';
            this.dispatchEvent(event);
        });

    }
}
document.registerElement('view-two', View2);