/// <reference path="./actions-creators/Action.ts" />
/// <reference path="./stores/Store.ts" />

import Store from './stores/Store'

//import html from './templates/main.html'

class Dispatcher {
    private stores: Array<Store> = [];

    dispatch(action: Action) {
        for (let store of this.stores) {
            store.accept(action);
        }
    }

    register(store: Store) {
        this.stores.push(store);
    }

}

export default new Dispatcher();