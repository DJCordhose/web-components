class View3 extends HTMLElement {
    static template = `
    <div>
      <h1>View3</h1>
        <button id="btn">Show Modal</button>
    </div>
    `;

    createdCallback() {
        this.createShadowRoot().innerHTML = View3.template;
        this.addHandler();
    }

    addHandler() {
        const component = this;
        const btn: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn');
        btn.addEventListener('click', (e: Event) => {
            const element = document.createElement('modal-view');
            document.body.appendChild(element);
            //component.appendChild(element);
            //const container = component.shadowRoot.querySelector('div');
            //container.appendChild(element);

            //const event: any = new Event("open");
            //event.viewId = 'modal-view';
            //// böse und falsch
            //event.nodeId = 'id4711';
            //this.dispatchEvent(event);
        });

    }


}
document.registerElement('view-three', View3);