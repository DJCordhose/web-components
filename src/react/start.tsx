///<reference path="../../typings/node/node.d.ts"/>
///<reference path="../../typings/react/react.d.ts"/>
///<reference path="../../typings/react/react-dom.d.ts"/>

import React = require('react');
import ReactDOM = require('react-dom');
//import React from 'react';
//import ReactDOM from 'react-dom';

// issues error, but can actually resolve the template
//import html = require('../templates/main.html');
//console.log(html);

import ReactFixedColumnTable from './ReactFixedColumnTable.tsx';

const titles = ["Spalte 1", "Spalte 2", "Spalte 3"];
const rows = [
    ["1.1", "1.2", "1.3", "1.4" ],
    ["2.1", "2.2", "2.3", "2.4" ]
];

const mountNode = document.getElementById('reactMountPoint');
ReactDOM.render(<ReactFixedColumnTable titles={titles} rows={rows}/>, mountNode);
