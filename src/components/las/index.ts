interface Document {
    registerElement: any;
}

interface HTMLElement {
    createShadowRoot: any;
    shadowRoot: any;
}

import './TreeView.ts';
import './PageView.ts';
import './MainView.ts';
import './View1.ts';
import './View2.ts';
import './View3.ts';
import './ModalView.ts';
