module.exports = {
    entry: "./src/las.ts",
    output: {
        path: __dirname,
        filename: "dist/bundle.js"
    },
    module: {
        loaders: [
            {test: /\.css$/, loader: "style!css"},
            {test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react' ]
                }},
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: 'ts-loader' },
            { test: /\.html?$/, loader: 'html' }

        ]
    },
    // Create Sourcemaps for the bundle
    devtool: 'source-map'
};
